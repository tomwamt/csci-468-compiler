import re

# Custom exception for when unexpected character is encountered
class ScanError(Exception):
	pass

class Token():
	'''Holds token data'''
	def __init__(self, typ, value):
		self.type = typ
		self.value = value

keywords = [
	'PROGRAM',
	'BEGIN',
	'ENDIF',
	'ENDWHILE',
	'END',
	'FUNCTION',
	'READ',
	'WRITE',
	'IF',
	'ELSE',
	'WHILE',
	'CONTINUE',
	'BREAK',
	'RETURN',
	'INT',
	'VOID',
	'STRING',
	'FLOAT'
]

# some operators escaped because they are also regex operators
operators = [
	r':=',
	r'\+',
	r'-',
	r'\*',
	r'/',
	r'=',
	r'!=',
	r'<=',
	r'<',
	r'>=',
	r'>',
	r'\(',
	r'\)',
	r';',
	r','
]

# Token definitions
tokens = [
	('COMMENT',	r'--[^\n]*(?=\n|$)'),
	('KEYWORD', str.join('|', (kw+r'\b' for kw in keywords))), # Join all the keywords postfixed with word boundary, separated by '|'
	('OPERATOR', str.join('|', operators)), # Join all the operators
	('FLOATLITERAL', r'[0-9]*\.[0-9]+'),
	('INTLITERAL', r'[0-9]+'),
	('STRINGLITERAL', r'\"[^\"]*\"'),
	('IDENTIFIER', r'[a-zA-Z][a-zA-Z0-9]*'),
	('WHITESPACE', r'[ \t\r\n]+'),
	('DEFAULT', r'.') # Anything not caught by the above is caught here
]

# Join the seperate regex into a long one with named groups
fullRegex = str.join('|', ('(?P<{}>{})'.format(name, regex) for name, regex in tokens))

def scan(inputString):
	'''
	Produces a stream of tokens from given code.
	Iteratively runs the input through a set of named regular expressions, yielding matches as Tokens

	Parameters:
	- inputString: program code, as a string

	Returns:
	- An iterable of Token objects
	'''

	# iterate through all matches
	for match in re.finditer(fullRegex, inputString):
		typ = match.lastgroup # name of group that matched
		value = match.group(typ) # value of match

		# if something was not caught by a real token
		if typ == 'DEFAULT':
			raise ScanError('Unexpected {}'.format(value))
		# else if it wasn't just whitespace or a comment
		elif typ != 'WHITESPACE' and typ != 'COMMENT':
			yield Token(typ, value) # return a new token
