from scanner import scan
from parser import parse
import actions

def generate_IR(tree, table):
	'''
	Produces IR instructions to implement the given parse tree.
	Uses a set of mutually recursive functions to generate instructions

	Parameters:
	- tree: a parse tree, as output from parser.parse
	- table: a global symbol table, as output from the actions module after
	parser.parse has been run

	Returns:
	- A list of IR instructions as strings
	'''

	# next temporary variable available for use
	currentReg = 0
	# collects IR instructions as they are emitted
	output = []
	# keeps track of which symbol table child index is next in the current and all parent scopes
	indexStack = []
	# used to generate unique labels for if & while statements
	currentLabel = 0

	### CODE GEN FUNCTIONS
	# traverses the parse tree to generate code
	# each takes a node in the parse tree and the symbol table of the current scope

	def gen_program(node, currTable):
		glbls, funcs = node[1]
		gen_decls(glbls, currTable)
		# as there should only be one function per file, funcs[0] should be the main function
		gen_decls(funcs[0][3][0], currTable.children[0]) # main function decls
		gen_stmt_list(funcs[0][3][1], currTable.children[0]) # main function stmts
		output.append('STOP')

	def gen_decls(node, currTable):
		for decl in node:
			if decl[0] == 'STRING':
				name = currTable.name+'_'+decl[1]
				value = decl[2]
				output.append(f'STRING {name} {value}')
			else:
				typ, names = decl
				for name in names:
					t = typ[0]
					val = currTable.name+'_'+name
					output.append(f'VAR{t} {val}')

	def gen_stmt_list(node, currTable):
		indexStack.append(0)
		for stmt in node:
			stmtType = stmt[0]
			if stmtType == 'ASSIGN':
				gen_assign(stmt, currTable)
			elif stmtType == 'READ':
				gen_read(stmt, currTable)
			elif stmtType == 'WRITE':
				gen_write(stmt, currTable)
			elif stmtType == 'RETURN':
				gen_return(stmt, currTable)
			elif stmtType == 'IF':
				gen_if_stmt(stmt, currTable)
			elif stmtType == 'WHILE':
				gen_while_stmt(stmt, currTable)
			else:
				print(stmt)
				raise RuntimeError('Something bad happened')
		indexStack.pop()

	# Stmt types
	def gen_assign(node, currTable):
		reg, typ = gen_expr(node[2], currTable)
		name = node[1]
		var, scope = currTable.get_var(name)
		fullname = scope.name + '_' + var.name
		output.append(f'STORE{typ} $T{reg} {fullname}')

	def gen_read(node, currTable):
		for varname in node[1]:
			var, scope = currTable.get_var(varname)
			typ = var.type[0]
			fullname = scope.name + '_' + var.name
			output.append(f'READ{typ} {fullname}')

	def gen_write(node, currTable):
		for varname in node[1]:
			var, scope = currTable.get_var(varname)
			typ = var.type[0]
			fullname = scope.name + '_' + var.name
			output.append(f'WRITE{typ} {fullname}')

	def gen_return(node, currTable):
		output.append("RETURN")

	def gen_if_stmt(node, currTable):
		nonlocal currentLabel
		label = currentLabel
		currentLabel += 1

		i = indexStack[-1]
		indexStack[-1] += 1
		gen_cond(node[1], currTable, f'ELSE_{label}')
		gen_decls(node[2], currTable.children[i])
		gen_stmt_list(node[3], currTable.children[i])

		output.append(f'JUMP OUT_{label}')
		output.append(f'LABEL ELSE_{label}')

		if node[4] is not None:
			gen_decls(node[4][0], currTable)
			gen_stmt_list(node[4][1], currTable.children[i+1])
			indexStack[-1] += 1

		output.append(f'LABEL OUT_{label}')

	def gen_while_stmt(node, currTable):
		nonlocal currentLabel
		label = currentLabel
		currentLabel += 1

		i = indexStack[-1]
		indexStack[-1] += 1
		output.append(f'LABEL LOOP_{label}')
		gen_cond(node[1], currTable, f'OUT_{label}')
		gen_decls(node[2], currTable.children[i])
		gen_stmt_list(node[3], currTable.children[i])
		output.append(f'JUMP LOOP_{label}')
		output.append(f'LABEL OUT_{label}')

	def gen_cond(node, currTable, label):
		r1, typ = gen_expr(node[0], currTable)
		r2, typ = gen_expr(node[2], currTable)
		if node[1] == '=':
			code = output.append(f'NE{typ} $T{r1} $T{r2} {label}')
		elif node[1] == '!=':
			code = output.append(f'EQ{typ} $T{r1} $T{r2} {label}')
		elif node[1] == '<':
			code = output.append(f'GE{typ} $T{r1} $T{r2} {label}')
		elif node[1] == '>':
			code = output.append(f'LE{typ} $T{r1} $T{r2} {label}')
		elif node[1] == '<=':
			code = output.append(f'GT{typ} $T{r1} $T{r2} {label}')
		elif node[1] == '>=':
			code = output.append(f'LT{typ} $T{r1} $T{r2} {label}')

	def gen_expr(node, currTable):
		nonlocal currentReg
		if isinstance(node, tuple):
			r1, t1 = gen_expr(node[0], currTable)
			r2, t2 = gen_expr(node[2], currTable)
			if node[1] == '+':
				output.append(f'ADD{t1} $T{r1} $T{r2} $T{currentReg}')
			elif node[1] == '*':
				output.append(f'MUL{t1} $T{r1} $T{r2} $T{currentReg}')
			elif node[1] == '-':
				output.append(f'SUB{t1} $T{r1} $T{r2} $T{currentReg}')
			elif node[1] == '/':
				output.append(f'DIV{t1} $T{r1} $T{r2} $T{currentReg}')
			currentReg += 1
			return currentReg -1, t1
		else:
			if node[0] in '0123456789':
				if '.' in node:
					typ = 'F'
				else:
					typ = 'I'
				val = node
			else:
				var, scope = currTable.get_var(node)
				typ = var.type[0]
				val = scope.name+"_"+var.name
			output.append(f'STORE{typ} {val} $T{currentReg}')
			currentReg += 1
			return currentReg - 1, typ

	# begin the traversal
	gen_program(tree, table)
	return output


def gen_tiny(ircode):
	'''
	Translates IR instructions into Tiny code

	Parameters:
	- ircode: a list of IR instructions, as strings

	Returns:
	- a list of Tiny instructions, as strings
	'''
	# list to contain variable declarations, which tiny requires to be at the begining of the code 
	decl = []
	# list to contain instructions
	output = []

	# iterates through the IR to translate each statment to Tiny
	# For most instructions this is a direct translation
	for ir in ircode:
		# split the IR into it's instruction and parameters
		parts = ir.split(' ')
		instr = parts[0]
		# Test against instruction name
		if instr == 'VARI' or instr == 'VARF':
			name = parts[1]
			decl.append(f'var {name}')
		elif instr == 'STRING':
			name = parts[1]
			value = str.join(' ', parts[2:])
			decl.append(f'str {name} {value}')
		elif instr == 'STOREI' or instr == 'STOREF':
			src = parts[1].replace('$T', 'r')
			dest = parts[2].replace('$T', 'r')
			output.append(f'move {src} {dest}')
		elif instr in ['NEI', 'EQI', 'GEI', 'LEI', 'GTI', 'LTI']:
			left = parts[1].replace('$T', 'r')
			right = parts[2].replace('$T', 'r')
			label = parts[3]
			output.append(f'cmpi {left} {right}')
			output.append(f'j{instr[:-1].lower()} {label}')
		elif instr in ['NEF', 'EQF', 'GEF', 'LEF', 'GTF', 'LTF']:
			left = parts[1].replace('$T', 'r')
			right = parts[2].replace('$T', 'r')
			label = parts[3]
			output.append(f'cmpr {left} {right}')
			output.append(f'j{instr[:-1].lower()} {label}')
		elif instr == 'LABEL':
			label = parts[1]
			output.append(f'label {label}')
		elif instr == 'JUMP':
			label = parts[1]
			output.append(f'jmp {label}')
		elif instr in ['ADDI', 'SUBI', 'MULI', 'DIVI']:
			left = parts[1].replace('$T', 'r')
			right = parts[2].replace('$T', 'r')
			result = parts[3].replace('$T', 'r')
			op = instr.lower()
			output.append(f'move {left} {result}')
			output.append(f'{op} {right} {result}')
		elif instr in ['ADDF', 'SUBF', 'MULF', 'DIVF']:
			left = parts[1].replace('$T', 'r')
			right = parts[2].replace('$T', 'r')
			result = parts[3].replace('$T', 'r')
			op = instr[:-1].lower() + 'r'
			output.append(f'move {left} {result}')
			output.append(f'{op} {right} {result}')
		elif instr == 'READI' or instr == 'WRITEI':
			op = instr.lower()
			v = parts[1].replace('$T', 'r')
			output.append(f'sys {op} {v}')
		elif instr == 'READF' or instr == 'WRITEF':
			op = instr.lower()[:-1] + 'r'
			v = parts[1].replace('$T', 'r')
			output.append(f'sys {op} {v}')
		elif instr == 'WRITES':
			name = parts[1]
			output.append(f'sys writes {name}')
		elif instr == 'STOP':
			output.append('sys halt')
		else:
			raise RuntimeError(f'Unexpected IR: {ir}')

	# prepend the declarations to the instructions
	return decl + output


if __name__ == '__main__':
	import sys

	# read the source code
	with open(sys.argv[1]) as f:
		code = f.read()

	# get the parse tree and symbol table from the code
	tree = parse(scan(code))
	tables = actions.symbolTables

	ir =  generate_IR(tree, tables[0])
	# print the IR commented first
	for line in ir:
		print(f'; {line}')
	# then print the tiny
	for line in gen_tiny(ir):
		print(line)
