import actions

def parse(tokens):
	'''
	Parses a stream of tokens and returns a simple parse tree (AST) according to the "tiny" grammer
	definition

	Parameters:
	- tokens: an iterable of token objects

	Returns:
	- The root node of a parse tree
	'''
	current = next(tokens)

	# advances the token stream
	def advance():
		nonlocal current
		try:
			current = next(tokens)
		except StopIteration:
			current = None

	# matches the current token against an expected token type, and optionally a value
	# if match, advance the token stream
	# if no match, raise an error
	def match(typ, value=None):
		# if value is None (omitted), treat as wildcard, and return actual value
		if value is None and current.type == typ:
			v = current.value
			advance()
			return v
		# otherwise match against both type and value
		if current.type == typ and current.value == value:
			advance()
			return value
		raise RuntimeError(f'Expected a {typ}( {value} ), got a {current.type}( {current.value} )')

	### RECURSIVE DESCENT FUNCTIONS
	# functions named for the rule they recognize
	# Each function will recognize its rule, returning the parse subtree that represents that rule.
	# Some functions may also call functions from the 'actions' module to process data
	# output notation used below:
	#	(...)     - a fixed-size tuple of subtrees
	#	[...]     - a variable-size list of subtrees
	#	... | ... - rule will output one of several types of subtrees
	#   '...'     - a literal string
	#	lowercase - a subtree produced by a rule
	#	UPPERCASE - the value of a matched token
	#	None      - an empty value

	def program():
		'''
		program -> PROGRAM IDENTIFIER BEGIN pgm_body END 
		output: (IDENTIFIER, pgm_body)
		'''
		match('KEYWORD', 'PROGRAM')
		idt = match('IDENTIFIER')
		match('KEYWORD', 'BEGIN')

		# creates a table for global variables
		actions.create_table('GLOBAL')

		body = pgm_body()
		match('KEYWORD', 'END')
		return (idt, body)

	def pgm_body():
		'''
		pgm_body          -> decl func_declarations
		output: (decl, func_declarations)
		'''
		d = decl()
		f = func_declarations()
		return (d, f)

	def decl():
		'''
		decl		      -> string_decl decl | var_decl decl | empty
		output: [string_decl | var_decl]
		'''
		result = []
		while current.type == 'KEYWORD' and current.value in ['STRING', 'INT', 'FLOAT']:
			if current.value == 'STRING':
				result.append(string_decl())
			elif current.value == 'INT' or current.value == 'FLOAT':
				result.append(var_decl())
		return result

	def string_decl():
		'''
		string_decl       -> STRING IDENTIFIER := STRINGLITERAL ;
		output: ('STRING', IDENTIFIER, STRINGLITERAL)
		'''
		match('KEYWORD', 'STRING')
		idt = match('IDENTIFIER')
		match('OPERATOR', ':=')
		val = match('STRINGLITERAL')
		match('OPERATOR', ';')

		actions.gen_str_decl(idt, val)

		return ('STRING', idt, val)

	def var_decl():
		'''
		var_decl          -> var_type id_list ;
		output: (var_type, id_list)
		'''
		t = var_type()
		i = id_list()

		actions.gen_decl(t, i)

		match('OPERATOR', ';')
		return (t, i)
		
	def var_type():
		'''
		var_type	        -> FLOAT | INT
		output: 'FLOAT' | 'INT'
		'''
		if current.type == 'KEYWORD' and (current.value in ['FLOAT', 'INT']):
			v = current.value
			advance()
			return v
		raise RuntimeError('Expected a FLOAT or INT')
		
	def any_type():
		'''
		any_type          -> var_type | VOID 
		output: 'FLOAT' | 'INT' | 'VOID'
		'''
		if current.type == 'KEYWORD' and (current.value in ['FLOAT', 'INT', 'VOID']):
			v = current.value
			advance()
			return v
		raise RuntimeError('Expected a FLOAT or INT or VOID')
		
	def id_list():
		'''
		id_list           -> id id_tail
		id_tail           -> , id id_tail | empty
		output: [IDENTIFIER]
		'''
		result = []
		result.append(match('IDENTIFIER'))
		while current.type == 'OPERATOR' and current.value == ',':
			advance()
			result.append(match('IDENTIFIER'))

		return result
			
	def param_decl_list():
		'''
		param_decl_list   -> param_decl param_decl_tail | empty
		param_decl_tail   -> , param_decl param_decl_tail | empty
		output: [param_decl]
		'''
		result = []
		while current.type == 'KEYWORD' and (current.value in ['FLOAT', 'INT']):
			result.append(param_decl())
			if current.type == 'OPERATOR' and current.value == ',':
				advance()
			else:
				break
		return result

	def param_decl():
		'''
		param_decl        -> var_type IDENTIFIER
		output: (var, IDENTIFIER)
		'''
		t = var_type()
		i = match('IDENTIFIER')

		actions.gen_decl(t, i)

		return (t, i)

	def func_declarations():
		'''
		func_declarations -> func_decl func_declarations | empty
		output: [func_decl]
		'''
		result = []
		while current.type == 'KEYWORD' and current.value == 'FUNCTION':
			f = func_decl()
			result.append(f)
		return result

	def func_decl():
		'''
		func_decl         -> FUNCTION any_type id (param_decl_list) BEGIN func_body END
		output: (any_type, IDENTIFIER, param_decl_list, func_body)
		'''
		match('KEYWORD', 'FUNCTION')
		ret = any_type()
		idt = match('IDENTIFIER')

		actions.create_table(idt)

		match('OPERATOR', '(')
		params = param_decl_list()
		match('OPERATOR', ')')
		match('KEYWORD', 'BEGIN')
		body = func_body()
		match('KEYWORD', 'END')

		actions.end_table()

		return (ret, idt, params, body)

	def func_body():
		'''
		func_body         -> decl stmt_list 
		output: (decl, stmt_list)
		'''
		d = decl()
		stmts = stmt_list()
		return (d, stmts)

	def stmt_list():
		'''
		stmt_list         -> stmt stmt_list | empty
		output: [stmt]
		'''
		result = []

		while current.type == 'IDENTIFIER' or (current.type == 'KEYWORD' and current.value in ['IF', 'WHILE', 'READ', 'WRITE', 'RETURN']):
			result.append(stmt())

		return result

	def stmt():
		'''
		stmt              -> base_stmt | if_stmt | while_stmt
		base_stmt         -> assign_stmt | read_stmt | write_stmt | return_stmt
		output: assign_stmt | read_stmt | write_stmt | return_stmt | base_stmt | if_stmt | while_stmt
		'''
		if current.type == 'KEYWORD' and current.value == 'IF':
			return if_stmt()
		if current.type == 'KEYWORD' and current.value == 'WHILE':
			return while_stmt()
		if current.type == 'KEYWORD' and current.value == 'READ':
			return read_stmt()
		if current.type == 'KEYWORD' and current.value == 'WRITE':
			return write_stmt()
		if current.type == 'KEYWORD' and current.value == 'RETURN':
			return return_stmt()
		if current.type == 'IDENTIFIER':
			return assign_stmt()

	def assign_stmt():
		'''
		assign_stmt       -> assign_expr ;
		assign_expr       -> IDENTIFIER := expr
		output: ('ASSIGN', IDENTIFIER, expr)
		'''
		idt = match('IDENTIFIER')
		match('OPERATOR', ':=')
		val = expr()
		match('OPERATOR', ';')
		return 'ASSIGN', idt, val

	def read_stmt():
		'''
		read_stmt         -> READ ( id_list );
		output ('READ', id_list)
		'''
		match('KEYWORD', 'READ')
		match('OPERATOR', '(')
		ids = id_list()
		match('OPERATOR', ')')
		match('OPERATOR', ';')
		return 'READ', ids

	def write_stmt():
		'''
		write_stmt        -> WRITE ( id_list );
		output ('WRITE', id_list)
		'''
		match('KEYWORD', 'WRITE')
		match('OPERATOR', '(')
		ids = id_list()
		match('OPERATOR', ')')
		match('OPERATOR', ';')
		return 'WRITE', ids

	def return_stmt():
		'''
		return_stmt       -> RETURN expr ;
		output: ('RETURN', expr)
		'''
		match('KEYWORD', 'RETURN')
		val = expr()
		match('OPERATOR', ';')
		return 'RETURN', val

	def expr():
		'''
		expr              -> expr_prefix factor
		expr_prefix       -> expr_prefix factor addop | empty
		output: (expr, addop, factor) | factor
		'''
		f1 = factor()
		while current.type == 'OPERATOR' and current.value in ['+', '-']:
			op = addop()
			f2 = factor()
			f1 = (f1, op, f2)
		return f1

	def factor():
		'''
		factor            -> factor_prefix postfix_expr
		factor_prefix     -> factor_prefix postfix_expr mulop | empty
		postfix_expr      -> primary | call_expr
		output: (factor, mulop, primary) | primary
		'''
		f1 = primary()
		while current.type == 'OPERATOR' and current.value in ['*', '/']:
			op = mulop()
			f2 = primary()
			f1 = (f1, op, f2)
		return f1

	def primary():
		'''
		primary           -> ( expr ) | IDENTIFIER | INTLITERAL | FLOATLITERAL
		call_expr         -> IDENTIFIER ( expr_list )
		output: expr | INTLITERAL | FLOATLITERAL | IDENTIFIER | (IDENTIFIER, expr_list)
		'''
		if current.type == 'OPERATOR' and current.value == '(':
			match('OPERATOR', '(')
			e = expr()
			match('OPERATOR', ')')
			return e
		elif current.type == 'INTLITERAL':
			return match('INTLITERAL')
		elif current.type == 'FLOATLITERAL':
			return match('FLOATLITERAL')
		elif current.type == 'IDENTIFIER':
			ident = match('IDENTIFIER')
			if current.type == 'OPERATOR' and current.value == '(':
				match('OPERATOR', '(')
				el = expr_list()
				match('OPERATOR', ')')
				return (ident, el)
			else:
				return ident

	def expr_list():
		'''
		expr_list         -> expr expr_list_tail | empty
		expr_list_tail    -> , expr expr_list_tail | empty
		output: [expr]
		'''
		result = []
		while not (current.type == 'OPERATOR' and current.value == ')'):
			result.append(expr())

			if current.type == 'OPERATOR' and current.value == ')':
				break

			match('OPERATOR', ',')
		return result

	def addop():
		'''
		addop             -> + | -
		output: '+' | '-'
		'''
		if current.type == 'OPERATOR' and current.value in ['+', '-']:
			return match('OPERATOR')
		else:
			raise RuntimeError('Expected an add operator')

	def mulop():
		'''
		mulop             -> * | /
		output: '*' | '/'
		'''
		if current.type == 'OPERATOR' and current.value in ['*', '/']:
			return match('OPERATOR')
		else:
			raise RuntimeError('Expected a multiply operator')

	def if_stmt():
		'''
		if_stmt           -> IF ( cond ) decl stmt_list else_part ENDIF
		output: ('IF', cond, decl, stmt_list, else_part)
		'''
		match('KEYWORD', 'IF')
		match('OPERATOR', '(')
		c = cond()
		match('OPERATOR', ')')

		actions.create_block_table()

		d = decl()
		stmts = stmt_list()

		actions.end_table()

		ep = else_part()
		match('KEYWORD', 'ENDIF')
		return 'IF', c, d, stmts, ep

	def else_part():
		'''
		else_part         -> ELSE decl stmt_list | empty
		output: (decl, stmt_list) | None
		'''
		if current.type == 'KEYWORD' and current.value == 'ELSE':
			match('KEYWORD', 'ELSE')

			actions.create_block_table()

			d = decl()
			stmts = stmt_list()

			actions.end_table()

			return (d, stmts)
		else:
			return None

	def cond():
		'''
		cond              -> expr compop expr
		output: (expr, compop, expr)
		'''
		left = expr()
		op = compop()
		right = expr()
		return (left, op, right)

	def compop():
		'''
		compop            -> < | > | = | != | <= | >=
		output: '<' | '>' | '=' | '!=' | '<=' | '>='
		'''
		if current.type == 'OPERATOR' and current.value in ['<', '>', '=', '!=', '<=', '>=']:
			return match('OPERATOR')
		else:
			raise RuntimeError('Expected a comparison operator')

	def while_stmt():
		'''
		while_stmt        -> WHILE ( cond ) decl stmt_list ENDWHILE
		output: ('WHILE', cond, decl, stmt_list)
		'''
		match('KEYWORD', 'WHILE')
		match('OPERATOR', '(')
		c = cond()
		match('OPERATOR', ')')

		actions.create_block_table()

		d = decl()
		stmts = stmt_list()

		actions.end_table()

		match('KEYWORD', 'ENDWHILE')
		return ('WHILE', c, d, stmts)

	# now that all the functions are defined, run the program recognizer.
	return program()
