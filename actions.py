currentTable = None
symbolTables = []

blockIndex = 1

# Custom exception for when a duplicate declaration is encountered
class DuplicateDeclError(Exception):
	def __init__(self, msg, v):
		super().__init__(msg)
		self.var = v

class SymbolTable:
	'''Represents a symbol table, a scope in which variables can be declared'''
	def __init__(self, name):
		self.name = name
		self.symbols = {}
		self.parent = None
		self.children = []

	def print(self):
		print(f'Symbol table {self.name}')
		for rname in self.symbols:
			self.symbols[rname].print()

	def get_var(self, name):
		'''Recursively searches for a variable with the given name'''
		if name in self.symbols:
			return self.symbols[name], self
		elif self.parent is not None:
			return self.parent.get_var(name)
		else:
			raise RuntimeError('No variable')

class VarRecord:
	'''Represents a variable declaration within a symbol table'''
	def __init__(self, name, typ):
		self.name = name
		self.type = typ
		self.value = None

	def print(self):
		if self.value is None:
			print(f'name {self.name} type {self.type}')
		else:
			print(f'name {self.name} type {self.type} value {self.value}')

def create_table(name):
	'''creates a table and sets it as current'''
	global currentTable

	table = SymbolTable(name)

	if currentTable is not None:
		currentTable.children.append(table)
		table.parent = currentTable

	currentTable = table
	symbolTables.append(table)

def create_block_table():
	'''creates a table and gives it an automatic name'''
	global blockIndex

	create_table(f'BLOCK_{blockIndex}')
	blockIndex += 1

def end_table():
	'''goes up a level'''
	global currentTable
	currentTable = currentTable.parent

def gen_decl(typ, names):
	'''creates an entry in the current symbol table'''
	for name in names:
		record = VarRecord(name, typ)

		if name in currentTable.symbols:
			raise DuplicateDeclError(f'{name} was already declared', name)

		currentTable.symbols[name] = record

def gen_str_decl(name, value):
	'''creates an entry for a string variable in the current symbol table'''
	record = VarRecord(name, 'STRING')
	record.value = value

	if name in currentTable.symbols:
			raise DuplicateDeclError(f'{name} was already declared', name)

	currentTable.symbols[name] = record
